FROM node:15.13-alpine
WORKDIR /testing-ci-cd-with-npm-project
ENV PATH="./node_modules/.bin:$PATH"
COPY . .
# Prepare the container for building React
RUN npm install
RUN npm install react-scripts@3.0.1 -g
# We want the production version
RUN npm run build
